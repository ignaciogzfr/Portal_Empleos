<%@page import="java.sql.ResultSet"%>
<%@page import="Model.Query"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="Controller.Login"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>HOME</title>
            <style type="text/css">
          .card-container{
            box-shadow: 0 0 5px rgba(0,0,0,.3);
            transition: all linear .2s;
            transform: translateY(0px);
          }

          .card-container:hover{
            box-shadow: 0 0 10px rgba(0,0,0,.6);
            transform: translateY(-5px);
          }
        </style>
    </head>
    <body>
                <nav class="navbar navbar-dark bg-dark">
  <a class="navbar-brand text-white">Avisos Clasificados</a>
  <form class="form-inline">
    <div class="text-white">
<%
    if (session.getAttribute("autenticado")==null){
        out.println("NO AUTENTICADO");
        out.println("<a href='loginregistro.jsp'  style='border:  3px solid white;' class='btn btn-dark btn-lg mb-2   rounded-0'>Ingresar</a>");
        out.println("<a href='loginregistro.jsp' style='border:  3px solid white;' class='btn btn-dark btn-lg mb-2   rounded-0'>Registrarse</a>");
    }else{
        out.println("SESION INICIADA");

  out.println("<a href='dashboard.jsp'  style='border:  3px solid white;' class='btn btn-dark btn-lg mb-2   rounded-0'>Mis Avisos</a>");
  out.println("<a href='logout.jsp'  style='border:  3px solid white;' class='btn btn-dark btn-lg mb-2   rounded-0'>Cerrar Sesion</a>");
    }
    
%>  
  </div>
  </form>
</nav>
        <div class="container-fluid" style="align-content: center; padding-top: 100px; padding-left: 300px; padding-right: 300px" class="btn-group">
            
         
            
                            
    <table  class="table table-dark mt-3 tabla-post">
                      <thead>
                        <tr>
                         
                          <th scope="col">Titulo</th>
                          <th scope="col">Descripcion</th>
                          <th scope="col">Ciudad</th>
                          <th scope="col">Categoria</th>
                          <th scope="col">Fecha</th>
                          <th scope="col">Fono</th>
                          <th scope="col">Email</th>
                        </tr>
                      </thead>
                      <tbody>
                        <!--Cargar la fecha se salta las weas-->
                     
                     
                            <% 
                              PreparedStatement pst = null;
                              ResultSet rs = null;
                              Query q = new Query();
                    pst= q.getConnection().prepareStatement("Select nombrePublicacion, Descripcion, nombreCiudad, nombreCategoria, fecha, Telefono , Email from publicacion"
                            + " INNER JOIN Ciudad on publicacion.idCiudad = Ciudad.idCiudad "
                            + " INNER JOIN Categoria on publicacion.idCategoria = Categoria.idCategoria"
                            + " INNER JOIN usuario on publicacion.idUsuario = usuario.idUsuario");
                        rs = pst.executeQuery();
                                while(rs.next()){
                            out.println("<tr><td id='ttitulo'>"+rs.getString(1)+"</td>");
                            out.println("<td id='ttitulo'>"+rs.getString(2)+"</td>");
                            out.println("<td id='ttitulo'>"+rs.getString(3)+"</td>");
                            out.println("<td id='ttitulo'>"+rs.getString(4)+"</td>");
                            out.println("<td id='ttitulo'>"+rs.getString(5)+"</td>");
                            out.println("<td id='ttitulo'>"+rs.getString(6)+"</td>");
                            out.println("<td id='ttitulo'>"+rs.getString(7)+"</td></tr>");
                            
                        }
                              
                              %>
                        
                            
                   
                
                        
                           
                          
                                                      
                        
                      </tbody>
            </table>
    
            
        </div>
    </body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function () {
      $('.tabla-post').DataTable({
          language: {
          processing:     "Procesando...",
          search:         "B�squeda",
          lengthMenu:     "Listar de _MENU_resultados",
          info:           "Listando de _START_ de _END_ de un total de  _TOTAL_ resultados",
          infoEmpty:      "Listando 0 resultados",
          infoFiltered:   "(Filtro de _MAX_ resultados totales)",
          infoPostFix:    "",
          loadingRecords: "Procesando",
          zeroRecords:    "No se encontr� ning�n resultado",
          emptyTable:     "Tabla vac�a",
          paginate: {
              first:      "Primero",
              previous:   "Anterior",
              next:       "Siguiente",
              last:       "�ltimo"
          },
          aria: {
              sortAscending:  ": Ordenar Ascendentemente",
              sortDescending: ": Ordenar Descendentemente"
          }
        }
      });
  } )
  </script>
</html>
