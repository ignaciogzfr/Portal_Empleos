<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.Query"%>
<!-- <%-- 
    Document   : dashboard
    Created on : 25-ago-2018, 19:03:48
    Author     : Nigger
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%> -->
<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
          .tabla-post{
            box-shadow: 0 0 5px rgba(0,0,0,.3);
            
            
          }

          .tabla-post:hover{
            box-shadow: 0 0 10px rgba(0,0,0,.6);
            
          }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dashboard</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    </head>
    <body>
<!-- Barra de Navegacion -->
            <nav class="navbar text-white navbar-dark bg-dark">
              <a class="navbar-brand">Avisos Clasificados</a>
              <form class="form-inline">
                <a class="btn btn-outline-light my-2 my-sm-0 mr-2" href="index.jsp">Home</a>
                <a class="btn btn-outline-light my-2 my-sm-0" type="submit" href="logout.jsp">Cerrar Sesion</a>
              </form>
            </nav>
<!-- Fin NavBar -->
<!-- Contenedor de toda la Pagina -->
    <div class="container" >





<!-- Titulo -->
            <div>
                <h1 class="text-center m-5">Wena loco</h1>
                <button class="btn-dark btn mb-3" data-toggle="modal" data-target="#exampleModal">Publicar Aviso</button>
            </div>

<!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Aviso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    <form method="GET" action="publicar">
                  <div class="modal-body">
                    
                          <div class="form-group">
                            <input type="text" name="titulo" class="form-control mb-3" id="modalTitulo" placeholder="Titulo Aviso">
                            <select name="ciudad" class="custom-select custom-select-light mb-3">
                             <option selected disabled>Ciudad</option>
                                 <%
                                     Query q = new Query();      
                    PreparedStatement  pst= null;
                    ResultSet  rs= null;
                    pst= q.getConnection().prepareStatement("Select * from ciudad");
                    rs = pst.executeQuery();
                        while(rs.next()){
                            out.println("<option id='ciudad"+rs.getString(1)+"'value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
                        }
                    %>
                              </select>
                              <select name="categoria" class="custom-select custom-select-light mb-3">
                              <option selected disabled>Categoria</option>
                          <% 
                         
                    pst= q.getConnection().prepareStatement("Select * from categoria");
                    rs = pst.executeQuery();
                        while(rs.next()){
                            out.println("<option id='categoria"+rs.getString(1)+"'value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
                        }
                          %>
                              </select>
                              <textarea placeholder="Ingrese Comentarios" name="descripcion" class="form-control" rows="3"></textarea>
                              <% out.println("<input type='hidden' name='idUsuario' value='"+session.getAttribute("idUsuario")+"'></input>");
                              %>
                          </div> 
                                
                        
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Cerrar</button>
                        <button class="btn  btn-primary rounded-0" type="submit">Guardar</button>
                    </div>
                </form>                      
                </div>
                </div>
            </div>

<!-- Modal Editar -->
            <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Editar Aviso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                    <form method="GET" action="update">
                  <div class="modal-body">
                    
                          <div class="form-group">
                            <input type="text" name="titulo" class="form-control mb-3" id="modalTitulo" placeholder="Titulo Aviso">
                            <select name="ciudad" class="custom-select custom-select-light mb-3">
                             <option selected disabled>Ciudad</option>
                                 <%
                    pst= q.getConnection().prepareStatement("Select * from ciudad");
                    rs = pst.executeQuery();
                        while(rs.next()){
                            out.println("<option id='ciudad"+rs.getString(1)+"'value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
                        }
                    %>
                              </select>
                              <select name="categoria" class="custom-select custom-select-light mb-3">
                              <option selected disabled>Categoria</option>
                          <% 
                         
                    pst= q.getConnection().prepareStatement("Select * from categoria");
                    rs = pst.executeQuery();
                        while(rs.next()){
                            out.println("<option id='categoria"+rs.getString(1)+"'value='"+rs.getString(1)+"'>"+rs.getString(2)+"</option>");
                        }
                          %>
                              </select>
                              <textarea placeholder="Ingrese Comentarios" name="descripcion" class="form-control" rows="3"></textarea>
                          </div> 
                                
                        
                </div>
                    <div class="modal-footer">
                        <input type="hidden" name="idPublicacion">
                        <button type="button" class="btn btn-secondary rounded-0" data-dismiss="modal">Cerrar</button>
                        <button class="btn  btn-primary rounded-0" type="submit">Editar</button>
                    </div>
                </form>                      
                </div>
                </div>
            </div>
<!-- Tabla de Posts -->
            <table  class="table table-dark mt-3 tabla-post">
                      <thead>
                        <tr>
                         
                          <th scope="col">Titulo</th>
                          <th scope="col">Descripcion</th>
                          <th scope="col">Ciudad</th>
                          <th scope="col">Categoria</th>
                          <th scope="col">Fecha</th>
                          <th scope="col" class="text-center">Acciones</th>
                         
                        </tr>
                      </thead>
                      <tbody>
                        <!--Cargar la fecha se salta las weas-->
                     
                     
                            <% 
                              String usuario = (String) session.getAttribute("idUsuario");
                    pst= q.getConnection().prepareStatement("Select nombrePublicacion, Descripcion, nombreCiudad, nombreCategoria, fecha, idPublicacion, publicacion.idCiudad, publicacion.idCategoria  from publicacion "
                            + " INNER JOIN Ciudad on publicacion.idCiudad = Ciudad.idCiudad "
                            + " INNER JOIN Categoria on publicacion.idCategoria = Categoria.idCategoria "
                            + "where publicacion.idUsuario = ? ");
                    pst.setString(1,usuario);
                        rs = pst.executeQuery();
                                while(rs.next()){
                            out.println("<tr><td id='Titulo'>"+rs.getString(1));
                            out.println("<td id='Desc'>"+rs.getString(2)+"</td>");
                            out.println("<td id='Ciudad'>"+rs.getString(3)+"</td>");
                            out.println("<td id='Categoria'>"+rs.getString(4)+"</td>");
                            out.println("<td id='Fecha'>"+rs.getString(5)+"</td>");
                            out.println("<td id='botonEliminar' class='text-center'><form method='GET' action='eliminar' id='form-delete'><input type='hidden' name='id' value='"+rs.getString(6)+"'><button type='submit'class='btn btn-danger btn-delete'><i class='fas fa-trash-alt'></i></button></form>");
                            out.println("<button data-toggle='modal' data-target='#modalEdit' class='btn btn-info btn-editar' idPublicacion='"+rs.getString(6)+"' nombrePublicacion='"+rs.getString(1)+"' descripcionPublicacion='"+rs.getString(2)+"' idCiudad='"+rs.getString(7)+"' idCategoria='"+rs.getString(8)+"'><i class='fas fa-edit'></i></button></td>");                            
                        }
                              
                              %>
                        
                            
                   
                
                        
                           
                          
                                                      
                        
                      </tbody>
            </table>
<!-- Fin Tabla -->
    <!-- Fin Container  -->
        </div>
            
</body>
    <!-- Scripts de BootStrap -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>
    <script>
       
    $(".btn-delete").on("click",function(event){
        event.preventDefault();
        
        swal({
            title: 'Estas seguro?',
            text: "No podras revertir este paso!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: "Cancelar"
       }).then((result) => {
         if (result.value) {
           $(this).closest("#form-delete").submit();
         }
       });
       
    })
    
        
    </script>
<script>
$(document).ready( function () {

      $(".btn-editar").on("click",function(){

        var id = $(this).attr("idPublicacion");
        var nombre =  $(this).attr("nombrePublicacion");
        var desc = $(this).attr("descripcionPublicacion");
        var ciudad = $(this).attr("idCiudad");
        var categoria = $(this).attr("idCategoria");

        $("#modalEdit input[name='titulo']").val(nombre);
        $("#modalEdit input[name='idPublicacion']").val(id);
        $("#modalEdit textarea[name='descripcion']").val(desc);
        $("#modalEdit select[name='categoria']").val(categoria);
        $("#modalEdit select[name='ciudad']").val(ciudad);

      })

      $('.tabla-post').DataTable({
          language: {
          processing:     "Procesando...",
          search:         "Búsqueda",
          lengthMenu:     "Listar de _MENU_resultados",
          info:           "Listando de _START_ de _END_ de un total de  _TOTAL_ resultados",
          infoEmpty:      "Listando 0 resultados",
          infoFiltered:   "(Filtro de _MAX_ resultados totales)",
          infoPostFix:    "",
          loadingRecords: "Procesando",
          zeroRecords:    "No se encontró ningún resultado",
          emptyTable:     "Tabla vacía",
          paginate: {
              first:      "Primero",
              previous:   "Anterior",
              next:       "Siguiente",
              last:       "Último"
          },
          aria: {
              sortAscending:  ": Ordenar Ascendentemente",
              sortDescending: ": Ordenar Descendentemente"
          }
        }
      });
  } )
  </script>
  
</html>
